'use strict'

/**
 * New Relic agent configuration.
 *
 * See lib/config.default.js in the agent distribution for a more complete
 * description of configuration variables and their potential values.
 */
var app = '';
switch (process.env['NODE_ENV']) {
  case 'dev':
    app = 'COMMONSERVICE-BACKEND-LOOPBACK-DEV';
    break;

  case 'qa':
    app = 'COMMONSERVICE-BACKEND-LOOPBACK-QA';
    break;

  case 'prod':
    app = 'COMMONSERVICE-BACKEND-LOOPBACK-PRODUCTION';
    break;

  default:
    app = 'COMMONSERVICE-BACKEND-LOOPBACK';
    break;
}

exports.config = {
  /**
   * Array of application names.
   */
  app_name: [app],
  /**
   * Your New Relic license key.
   */
  license_key: '50243424e2c26e017828720d1e793beec0209ebd',
  logging: {
    /**
     * Level at which to log. 'trace' is most useful to New Relic when diagnosing
     * issues with the agent, 'info' and higher will impose the least overhead on
     * production applications.
     */
    level: 'info'
  }
}
