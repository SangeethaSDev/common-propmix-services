var constant = require('../constant.json')[process.env.NODE_ENV || 'dev'];
var request = require('request');
var AWS = require('aws-sdk'),
    fs = require('fs'),
    path = require('path');
module.exports = function(server) {
    server.get('/getPublicData', function(req, res) {

        var condition = {};
        condition["Street"] = req.query.Street;
        condition["City"] = req.query.City;
        condition["State"] = req.query.State;
        condition["Zip"] = req.query.Zip;
        var data = {
            "UnparsedAddress": '',
            "City": "",
            "StateOrProvince": "",
            "PostalCode": "",
            "PostalCodePlus4": "",
            "ParcelNumber": "",
            "PropertyType": "",
            "PropertySubType": "",
            "LotSizeSquareFeet": "",
            "LivingArea": "",
            "ArchitecturalStyle": "",
            "Heating": "",
            "Cooling": "",
            "StoriesTotal": "",
            "StoriesDescription": "",
            "YearBuilt": "",
            "Roof": "",
            "ConstructionMaterials": "",
            "BedroomsTotal": "",
            "BathroomsTotalInteger": "",
            "ParkingFeatures": "",
            "PoolFeatures": "",
            "View": "",
            "PatioAndPorchFeatures": "",
            "Basement": "",
            "FireplacesTotal": "",
            "FireplaceYN": "",
            "FireplaceFeatures": "",
            "InteriorFeatures": "",
            "ExteriorFeatures": "",
            "OtherStructures": "",
            "PublicRemarks": "",
            "LotFeatures": "",
            "ZoningDescription": "",
            "CommunityFeatures": "",
            "ElementarySchoolDistrict": "",
            "MiddleOrJuniorSchoolDistrict": "",
            "HighSchoolDistrict": "",
            "ElementarySchool": "",
            "MiddleOrJuniorSchool": "",
            "HighSchool": "",
            "Appliances": "",
            "LotSizeDimensions": "",
            "Topography": "",
            "WaterSource": "",
            "ListingContractDate": "",
            "MlsStatus": "",
            "ListPrice": "",
            "ClosePrice": "",
            "CloseDate": "",
            "StandardStatus": "",
            "DistressedYN": "",
            "DistressType": "",
            "GarageYN": "",
            "GarageSpaces": "",
            "ListAgentStateLicense": "",
            "ListAgentFullName": "",
            "ListAgentPreferredPhone": "",
            "ListAgentEmail": "",
            "ListOfficeName": "",
            "ListOfficePhone": "",
            "ListOfficeEmail": "",
            "ListingId": "",
            "ListingKey": "",
            "ModificationTimestamp": "",
            "FIPS": "",
            "PropertyRefID": "",
            "PriceperSquareFeet": "",
            "WaterfrontFeatures": "",
            "WaterYN": "",
            "DaysOnMarket": "",
            "ListingType": "",
            "MLSListingNumber": "",
            "AgentAddress": "",
            "OfficeAddress1": "",
            "Address": "",
            "Latitude": "",
            "Longitude": "",
            "BathroomsDecimal": "",
            "WaterAccess": "",
            "RoomsTotal": "",
            "Distance": "",
            "SubjectProperty": "",
            "ImageCount": 0,
            "ImageURLs": ["No image URL available for this property"]
        };
        try {
            condition['Street'] = condition['Street'].replace(/#/, '%23');
            var publicDataQuery = '&StreetName=' + condition['Street'] + '&State=' + condition['State'] + '&City=' + condition['City'] + '&ZipCode=' + condition['Zip'];
            var publicDataUrl = constant.publicRecordApi + publicDataQuery;
            request.get({ url: encodeURI(publicDataUrl) }, function(err, httpResponse, body) {
                var PBjson, PBjsonRes;
                if (body && body.charAt(0) === '{' && body.charAt(body.length - 1) === '}') {
                    PBjsonRes = JSON.parse(body);
                }
                if (!err) {
                    if (PBjsonRes.ErrorCode) {
                        res.send(PBjsonRes)
                    } else if (PBjsonRes.ErrorStatusCode) {
                        res.send(PBjsonRes)
                    } else {
                        PBjson = { 'Property': PBjsonRes, 'publicData': true };
                        var keyForCompare = Object.keys(PBjson.Property)
                        var keysForData = Object.keys(data)
                        var dataUnderscoreKeys = keysForData.filter(function(x) {
                            var checker = x.replace(/(?:^|\.?)([A-Z])/g, function(x, y) { return "_" + y.toUpperCase() }).replace(/^_/, "");
                            if (keyForCompare.includes(checker)) {
                                return x;
                            }
                        })
                        keyForCompare.filter(function(x) {
                            if (keysForData.includes(x)) {
                                data[x] = PBjson.Property[x];
                            } else {
                                if (dataUnderscoreKeys.includes(x)) {
                                    data[x] = PBjson.Property[x];
                                }
                            }
                        })
                        data["PostalCode"] = PBjsonRes.PostalCode ? PBjsonRes.PostalCode : "";
                        data["PostalCodePlus4"] = PBjsonRes.PostalCodePlus4 ? PBjsonRes.PostalCodePlus4 : "";
                        data["PropertyType"] = PBjsonRes.PropertyType ? PBjsonRes.PropertyType : "";
                        data["StoriesTotal"] = PBjsonRes.Stories ? PBjsonRes.Stories : "";
                        data["StoriesDescription"] = PBjsonRes.StoriesDescription ? PBjsonRes.StoriesDescription : "";
                        data["Roof"] = PBjsonRes.Roof_Type ? PBjsonRes.Roof_Type : "";
                        data["ConstructionMaterials"] = PBjsonRes.Construction_Type ? PBjsonRes.Construction_Type : "";
                        data['LivingArea'] = PBjsonRes.Living_Area ? PBjsonRes.Living_Area : ''
                        var bathCount = ''
                        if (PBjsonRes.Bathrooms_Full)
                            bathCount = ''
                        if (!PBjsonRes.Bathrooms_Half)
                            bathCount = PBjsonRes.Bathrooms_Full

                        if (PBjsonRes.Bathrooms_Full && PBjsonRes.Bathrooms_Full != '' && parseInt(PBjsonRes.Bathrooms_Full) >= 0) {
                            bathCount = parseInt(PBjsonRes.Bathrooms_Full)
                        }
                        if (PBjsonRes.Bathrooms_Half && PBjsonRes.Bathrooms_Half != '' && parseInt(PBjsonRes.Bathrooms_Half) >= 0) {
                            if (bathCount != '' && parseInt(bathCount) >= 0) {
                                bathCount = parseInt(bathCount) + parseInt(PBjsonRes.Bathrooms_Half);
                            } else {
                                bathCount = parseInt(PBjsonRes.Bathrooms_Half)
                            }
                        }

                        data["BathroomsTotalInteger"] = bathCount && bathCount > 0 ? bathCount : "";
                        data["BedroomsTotal"] = PBjsonRes.Beds && PBjsonRes.Beds != '' && parseInt(PBjsonRes.Beds) > 0 ? PBjsonRes.Beds : "";
                        data["YearBuilt"] = PBjsonRes.Year_Built && PBjsonRes.Year_Built != '' ? PBjsonRes.Year_Built : "";

                        data["GarageSpaces"] = PBjsonRes.GarageSpaces ? PBjsonRes.GarageSpaces : "";
                        data["ParkingFeatures"] = PBjsonRes.Parking_Type ? PBjsonRes.Parking_Type : '';
                        data["PatioAndPorchFeatures"] = (PBjsonRes.Porch_Type || PBjsonRes.Patio_Type) ? PBjsonRes.Porch_Type + " " + PBjsonRes.Patio_Type : "";
                        data["Basement"] = PBjsonRes.Basement_Area ? PBjsonRes.Basement_Area : "";
                        data["FireplaceYN"] = PBjsonRes.Fireplace_YN ? PBjsonRes.Fireplace_YN : "";
                        data["ZoningDescription"] = PBjsonRes.Zoning ? PBjsonRes.Zoning : "";
                        data["GarageYN"] = PBjsonRes.GarageSpaces != " " ? "Y" : "N";
                        data["WaterYN"] = (PBjsonRes.WaterSource && PBjsonRes.WaterSource != "") ? "Y" : "N";
                        data["Address"] = PBjsonRes.Standardized_Address ? PBjsonRes.Standardized_Address : "";
                        var AddressArray = data['Address'].split(',');
                        if (AddressArray[2])
                            data['PostalCode'] = AddressArray[2].match(/\d+/)[0];;
                        data["BathroomsDecimal"] = bathCount;
                        data["WaterAccess"] = PBjsonRes.WaterSource ? PBjsonRes.WaterSource : "";
                        data['PropertyType'] = PBjsonRes.County_Use ? PBjsonRes.County_Use : "";
                        data['PropertySubType'] = PBjsonRes.Land_Use ? PBjsonRes.Land_Use : "";
                        data['LotSizeSquareFeet'] = PBjsonRes.LotSizeArea ? PBjsonRes.LotSizeArea : "";
                        var taxConv = ''
                        if (PBjsonRes.Property_Tax)
                            taxConv = parseFloat(PBjsonRes.Property_Tax);
                        data['TaxAnnualAmount'] = taxConv;
                        data['Sewer'] = PBjsonRes.Sewer ? PBjsonRes.Sewer : '';
                        data['County'] = PBjsonRes.County ? PBjsonRes.County : '';
                        data['Style'] = data['ArchitecturalStyle'];
                        data['UnparsedAddress'] = PBjsonRes.UnparsedAddress ? PBjsonRes.UnparsedAddress : ''
                        var wordChecker = ''
                        // add into the word array if new abbrevations/names comes for respective property sub type
                        var possibleKeys = [
                            { "Manufactured Home": { word: ['Manufactured Modular'] } },
                            { "Manufactured on Land": { word: ['Site Built', 'Site BLT'] } },
                            { "Mobile Home": { word: ['Mobile'] } },
                            { "Multi Family>4": { word: ['Multi Family'] } },
                            { "Single Family Residence": { word: ['SFR', 'RESIDENTIAL (NEC)'] } },
                            { "Stock Cooperative": { word: ['Cooperative', 'Coop'] } },
                            { "Apartment": { word: ['Apartment', 'Apt'] } },
                            { "Cabin": { word: ['Cabin'] } },
                            { "Condominium": { word: ['Condominium', 'Condo'] } },
                            { "Duplex": { word: ['Duplex'] } },
                            { "Timeshare": { word: ['Timeshare'] } },
                            { "Townhouse": { word: ['Townhouse', 'Rowhouse'] } },
                            { "Triplex": { word: ['Triplex'] } },
                            { "Residential": { word: [data['PropertySubType']] } }
                        ];
                        var dataSwitched = false;
                        possibleKeys.find(function(x) {
                            if (!dataSwitched) {
                                wordChecker = x[Object.keys(x)].word;
                                wordlist = wordChecker.find(function(y) {
                                    y = y.toLowerCase();
                                    if (y.includes(data['PropertySubType'].toLowerCase())) {
                                        data['PropertySubType'] = Object.keys(x)[0];
                                        dataSwitched = true;
                                    }
                                })
                            }
                        })
                        var result = { 'ListingDetails': [data], 'publicData': true };
                        res.send(result)
                    }
                } else {
                    var internalGeoCodeData = constant.internalGeoCodeApi + "Street=" + condtion.Street + "&City=" + condtion.City + "&State=" + condtion.State + "&Zip=" + condtion.Zip
                    request.get({ url: encodeURI(internalGeoCodeData) }, function(err, httpResponse, body) {
                        var geoJsonRes;
                        if (body && body.charAt(0) === '{' && body.charAt(body.length - 1) === '}') {
                            geoJsonRes = JSON.parse(body);
                        }
                        if (!err) {
                            var resultObj = { 'Property': geoJsonRes, 'publicData': false };
                            data["Address"] = resultObj.StandardizedAddress;
                            data["PostalCode"] = resultObj.StandardizedAddressComponents.Zip;
                            data['PostalCodePlus4'] = resultObj.StandardizedAddressComponents.Zip4;
                            data['City'] = resultObj.StandardizedAddressComponents.City;
                            data['StateOrProvince'] = resultObj.StandardizedAddressComponents.State;
                            data['Latitude'] = resultObj.Geocode.Latitude;
                            data['Longitude'] = resultObj.Geocode.Longitude;
                            var result = { 'Listings': data, 'publicData': false };
                            res.json(result)
                        } else {
                            res.json({ 'error': 'geo Json error' });
                        }
                    });
                }
            });
        } catch (e) {
            res.json({ 'error': '500' });
        }


    });
    server.post('/uploadData', function(req, resp) {

        var uploadObj = req.body;
        var d = new Date();
        var utcDate = d.getTime();
        if (!uploadObj || !uploadObj.project || !uploadObj.env || !uploadObj.url || uploadObj.project == '' || uploadObj.env == '' || uploadObj.url == '' || !uploadObj.type || uploadObj.type != 'base64' || !uploadObj.ext || uploadObj.ext == '') {
            resp.json({ 'status': 500, 'Message': 'Please verify upload data.' });
        } else {


            var pathTosave = uploadObj.project + '/' + uploadObj.env + '/' + utcDate + '.' + uploadObj.ext;

            AWS.config.update({ accessKeyId: constant.accessKeyId, secretAccessKey: constant.secretAccessKey });
            if (uploadObj.type == 'base64') {

                var buf = new Buffer(uploadObj.url.replace(/^data:image\/\w+;base64,/, ""), 'base64')
                var s3 = new AWS.S3();
                s3.upload({
                    Bucket: 'static-propmix-io',
                    Key: pathTosave,
                    Body: buf,
                    ACL: 'public-read'
                }, function(response, params) {
                    if(params && params.Location){
                        
                        var urlArray=params.Location.split('https://static-propmix-io.s3.amazonaws.com');
                        if(urlArray[1] && urlArray[1]!='' && urlArray[1]!=undefined){
                            params.Location='https://statictest.propmix.io'+urlArray[1];
                            
                        }

                    }
                    params.status=200;
                    resp.json(params);
                });
            } else {
                resp.json({ 'status': 500, 'Message': 'Please provide base64Data.' });
            }
        }

    });
};